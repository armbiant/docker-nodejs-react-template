import {defineConfig} from 'vite'
import {VitePluginNode} from 'vite-plugin-node';

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 3000,
  },
	plugins: [...VitePluginNode({
		adapter: 'fastify',
		appPath: './src/main.ts',
		exportName: 'mainSrv',
		tsCompiler: 'esbuild',
	})],
});
