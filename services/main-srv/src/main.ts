import fastify from 'fastify';

const app = async () => {
	const app = fastify();

	app.get('/', (_req, reply) => {
		reply.send({hello: 'World'});
	});

	app.get('/ping', (_req, reply) => {
		reply.send({ msg: 'pong' });
	});

	app.get('/pong', (_req, reply) => {
		reply.send({ msg: 'ping' });
	});

	app.listen({ port: 3000, host: '0.0.0.0' });
	return app;
};

export const mainSrv = app();