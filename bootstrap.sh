#!/usr/bin/env bash

if ! command -v yarn &> /dev/null
then
    echo "yarn could not be found. Attempt to enable it."
    corepack enable

    if ! command -v yarn &> /dev/null
    then
    	echo "The automatic yarn installation not possible. Please solve issue manually and re-run bootstrap script."
    	exit 1
    fi
fi

YARN_VERSION="$(yarn -v)"

if (( $(echo "$YARN_VERSION 3.4.1" | awk '{print ($1 < $2)}') )); then
	echo "Updating yarn to 3.4.1"
	yarn set version 3.4.1
	echo "Yarn version upgraded..."
	echo ""
else
	echo "Yarn detected in correct version"
	echo ""
fi

if [ ! -f "./.env.locale" ]; then
	echo ".env.locale file doesn't exist. Copying values from template"
	cp ./.env.template ./.env.locale
	echo "./env.locale file created successfully..."
	echo ""
else
	echo ".env.locale file exist, no additional actions required"
	echo "Warning! The script doesn't compare or migrate .env files"
	echo ""
fi

if [ ! -f "./.env.dev.locale" ]; then
	echo ".env.dev.locale file doesn't exist. Copying values from template"
	cp ./.env.template ./.env.dev.locale
	echo "./env.dev.locale file created successfully..."
	echo ""
else
	echo ".env.dev.locale file exist, no additional actions required"
	echo "Warning! The script doesn't compare or migrate .env files"
	echo ""
fi
